package com.tbclec.androidrccar;

/**
 * Created by teo on 15.05.2016.
 */
public class Constants {

    public static final String TAG_TSS = "TTS-";
    public static final String TAG_OLIVIA = "OLIV-";
    public static final String TAG_SONG = "SONG-";

    public static final int RC_INSTALL_TTS_DATA = 2001;

    public static final int REQ_CODE_ACCESS_FINE_LOCATION = 2;
    public static final int REQ_CODE_RECORD_AUDIO = 3;

}
