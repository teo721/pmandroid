package com.tbclec.androidrccar;

import android.bluetooth.BluetoothSocket;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

class ConnectedThread extends Thread {

	private final BluetoothSocket mSocket;
	private final InputStream mInStream;
	private final OutputStream mOutStream;

	public ConnectedThread(BluetoothSocket socket, MainActivity mainActivity) {

		mSocket = socket;
		InputStream tmpIn = null;
		OutputStream tmpOut = null;

		// Get the input and output streams, using temp objects because member streams are final
		try {
			tmpIn = socket.getInputStream();
			tmpOut = socket.getOutputStream();
		}
		catch (IOException e) {
			e.printStackTrace();
		}

		mInStream = tmpIn;
		mOutStream = tmpOut;

		mainActivity.mConnectedThread = this;
		mainActivity.setIsConnected(true);
	}

	public void run() {

		byte[] buffer = new byte[1024];  // buffer store for the stream
		int bytes; // bytes returned from read()

		// Keep listening to the InputStream until an exception occurs
		while (true) {
			try {
				// Read from the InputStream
				bytes = mInStream.read(buffer);

				// Do something with the result
			}
			catch (IOException e) {
				break;
			}
		}
	}

	/* Call this from the main activity to send data to the remote device */
	public void write(byte[] bytes) {

		try {
			mOutStream.write(bytes);
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	/* Call this from the main activity to shutdown the connection */
	public void cancel() {

		try {
			mSocket.close();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
}
