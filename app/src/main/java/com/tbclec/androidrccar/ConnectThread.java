package com.tbclec.androidrccar;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.util.Log;
import android.widget.ImageView;

import java.io.IOException;
import java.util.UUID;

class ConnectThread extends Thread {

	// TODO: Generate your own unique identifier
	static final UUID BT_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

	private final BluetoothSocket mSocket;
	private BluetoothAdapter mBluetoothAdapter;
	private MainActivity mMainActivity;

	public ConnectThread(BluetoothDevice device, MainActivity mainActivity) {

		// Use a temporary object that is later assigned to mSocket, because mSocket is final
		BluetoothSocket tmp = null;
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

		// Get a BluetoothSocket to connect with the given BluetoothDevice
		try {
			// MY_UUID is the app's UUID string, also used by the server code
			tmp = device.createRfcommSocketToServiceRecord(BT_UUID);
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		mSocket = tmp;

		mMainActivity = mainActivity;
	}

	public void run() {

		// Cancel discovery because it will slow down the connection
		mBluetoothAdapter.cancelDiscovery();

		try {
			// Connect the device through the socket. This will block
			// until it succeeds or throws an exception
			mSocket.connect();
		}
		catch (IOException connectException) {
			connectException.printStackTrace();
			Log.d("BT", "Unable to connect.");

			// Unable to connect; close the socket and get out
			try {
				mSocket.close();
			}
			catch (IOException closeException) {
				closeException.printStackTrace();
			}
			return;
		}

		mMainActivity.setSocket(mSocket);

		// Set status image.
		final ImageView img = (ImageView) mMainActivity.findViewById(R.id.status);
		img.post(new Runnable() {
			public void run() {
				img.setImageResource(R.drawable.ok);
			}
		});

		// Do work to manage the connection (in a separate thread)
		new ConnectedThread(mSocket, mMainActivity).start();
	}

	/**
	 * Will cancel an in-progress connection, and close the socket
	 */
	public void cancel() {
		try {
			mSocket.close();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
}
