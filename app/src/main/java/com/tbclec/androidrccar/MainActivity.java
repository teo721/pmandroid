package com.tbclec.androidrccar;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

	static final String MAC = "20:16:03:25:13:11";

	BluetoothAdapter mBluetoothAdapter;
	BluetoothSocket mSocket;

	ConnectedThread mConnectedThread;
	ConnectThread mConnectThread;
	boolean mIsConnected;

	private enum MicState {DISABLED, ON, RECORDING}



	private final Object voiceListeningSync = new Object();

	private boolean activityRecreated;
	private TextToSpeech textToSpeech;
	private SpeechRecognizer speechRecognizer;
	private boolean voiceListeningStopped = true;

	private TextView questionView;


	private boolean permission_ACCESS_FINE_LOCATION() {

		List<String> listPermissionsNeeded = new ArrayList<>();

		int permissionGetAccounts = ContextCompat.checkSelfPermission(this,
				Manifest.permission.ACCESS_FINE_LOCATION);

		if (permissionGetAccounts != PackageManager.PERMISSION_GRANTED) {
			listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
		}

		if (!listPermissionsNeeded.isEmpty()) {
			ActivityCompat.requestPermissions(this,
					listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),
					Constants.REQ_CODE_ACCESS_FINE_LOCATION);
			return false;
		}
		return true;
	}

	private boolean permission_RECORD_AUDIO() {

		List<String> listPermissionsNeeded = new ArrayList<>();

		int permissionGetAccounts = ContextCompat.checkSelfPermission(this,
				Manifest.permission.RECORD_AUDIO);

		if (permissionGetAccounts != PackageManager.PERMISSION_GRANTED) {
			listPermissionsNeeded.add(Manifest.permission.RECORD_AUDIO);
		}

		if (!listPermissionsNeeded.isEmpty()) {
			ActivityCompat.requestPermissions(this,
					listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),
					Constants.REQ_CODE_RECORD_AUDIO);
			return false;
		}
		return true;
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

		switch (requestCode) {
			case Constants.REQ_CODE_ACCESS_FINE_LOCATION: {

				// If request is cancelled, the result arrays are empty.
				if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					// permission was granted, yay! Do the
					// contacts-related task you need to do.
					initConection();
				}
				else {
					// permission denied, boo! Disable the
					// functionality that depends on this permission.
				}
			}
			case  Constants.REQ_CODE_RECORD_AUDIO: {
				if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					// permission was granted, yay! Do the
					// contacts-related task you need to do.
					initSpeechRecognizer();
				}
				else {
					// permission denied, boo! Disable the
					// functionality that depends on this permission.
				}
			}
		}
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		activityRecreated = savedInstanceState != null;
		questionView = (TextView) findViewById(R.id.question_view);
		// Set listener for up_button
		Button button = (Button) this.findViewById(R.id.up_button);
		button.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {

				if (mConnectedThread == null) {
					return true;
				}

				v.onTouchEvent(event);
				if (event.getAction() == MotionEvent.ACTION_UP) {
					mConnectedThread.write("0".getBytes());
					return true;
				}
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					mConnectedThread.write("1".getBytes());
					return true;
				}

				return false;
			}
		});
		Button f = (Button) this.findViewById(R.id.forward);

		f.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v)
			{
				mConnectedThread.write("f".getBytes());
			}
		});
	}

	public void onStart() {
		super.onStart();
	}

	public void onResume() {
		super.onResume();
		if(permission_ACCESS_FINE_LOCATION()) {
			initConection();
		}
		if(permission_RECORD_AUDIO()) {
			initSpeechRecognizer();
		}
	}

	public void onPause() {
		super.onPause();

		clearConnection();
		clearSpeechRecognizer();
	}

	public void onStop() {
		super.onStop();
	}

	public void onDestroy() {
		super.onDestroy();
	}

	private void initConection() {

		mIsConnected = false;

		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		if (mBluetoothAdapter == null) {
			Log.d("BT", "Bluetooth is disabled on the Android device.");
			Toast.makeText(this, "Device does not support Bluetooth.", Toast.LENGTH_LONG).show();
			return;
		}

		if (!mBluetoothAdapter.isEnabled()) {
			Log.d("BT", "Bluetooth is disabled on the Android device.");
			Toast.makeText(this, "Bluetooth is disabled on the Android device.", Toast.LENGTH_LONG).show();
			return;
		}

		ImageView statusImg = (ImageView) findViewById(R.id.status);
		statusImg.setImageResource(R.drawable.not_ok);

		// Connect to the HC-05 module
		BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(MAC);

		if (device == null) {
			Toast.makeText(this, "Unable to find bluetooth module.", Toast.LENGTH_LONG).show();
			return;
		}

		mConnectThread = new ConnectThread(device, MainActivity.this);
		mConnectThread.start();
	}

	private void clearConnection() {

		if (mConnectedThread != null) {
			mConnectedThread.cancel();
			mConnectedThread = null;
		}
		if (mConnectThread != null) {
			mConnectThread.cancel();
			mConnectThread = null;
		}
	}

	public void setSocket(BluetoothSocket sock) {

		mSocket = sock;
	}

	public void setIsConnected(boolean val) {

		mIsConnected = val;
	}

	private void setMicStatus(MicState micState) {
		ImageView askButton = (ImageView) findViewById(R.id.ask_button);
		switch (micState) {
			case DISABLED:
				askButton.setColorFilter(Color.GRAY);
				askButton.setEnabled(false);
				break;
			case ON:
				askButton.setColorFilter(Color.BLACK);
				askButton.setEnabled(true);
				break;
			case RECORDING:
				askButton.setColorFilter(Color.RED);
				askButton.setEnabled(false);
				break;
			default:
				askButton.setColorFilter(Color.BLACK);
				askButton.setEnabled(true);
		}
	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (requestCode == Constants.RC_INSTALL_TTS_DATA) {

			if (resultCode == RESULT_OK) {
				setMicStatus(MicState.ON);
			}
			else {
				Log.d("error liste", "TTS ERROR - result code: " + resultCode);
			}
		}
	}

// -------------------------------------------------------------------------------------------------
// SPEECH RECOGNIZER ACTIONS
// -------------------------------------------------------------------------------------------------

	private void startVoiceListening() {

		synchronized (voiceListeningSync) {

			voiceListeningStopped = false;

			Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
			intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
			intent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, this.getPackageName());

			speechRecognizer.startListening(intent);
			Log.d(Constants.TAG_TSS, "startVoiceListening");

			new CountDownTimer(2000, 1000) {

				@Override
				public void onTick(long millisUntilFinished) {
					//do nothing, just let it tick
				}

				@Override
				public void onFinish() {
					stopVoiceListening();
				}
			}.start();
			setMicStatus(MicState.RECORDING);
		}
	}

	private void stopVoiceListening() {

		synchronized (voiceListeningSync) {

			if (voiceListeningStopped) {
				return;
			}
			voiceListeningStopped = true;

			speechRecognizer.stopListening();
			Log.d(Constants.TAG_TSS, "stopVoiceListening");
			setMicStatus(MicState.ON);
		}
	}
	private void processSTT(String result){
		if (mConnectedThread == null) {
			return;
		}

		switch (result) {
			case "right":
				mConnectedThread.write("r".getBytes());
				break;
			case "left":
				mConnectedThread.write("l".getBytes());
				break;
			case "forward":
				mConnectedThread.write("f".getBytes());
			case "up":
				mConnectedThread.write("f".getBytes());
				break;
			case "backward":
				mConnectedThread.write("b".getBytes());
				break;
			case "back":
				mConnectedThread.write("b".getBytes());
				break;
			case "stop":
				mConnectedThread.write("s".getBytes());
				break;
			case "rotate":
				mConnectedThread.write("o".getBytes());
				break;
			default:
				break;
		}

	}
	private void notifyVoiceListeningResultReady(ArrayList<String> results) {

		stopVoiceListening();
		String result_string = "";
		for(String result: results){
			result_string += result + "\n";
			processSTT(result);
		}
		questionView.setText(getString(R.string.question) + " " + result_string);

	}

// -------------------------------------------------------------------------------------------------
// LISTENERS
// -------------------------------------------------------------------------------------------------

	public void buttonAsk(View v) {

		startVoiceListening();
	}


	private RecognitionListener recognitionListener = new RecognitionListener() {

		@Override
		public void onBeginningOfSpeech() {
			Log.d("Speech", "onBeginningOfSpeech");
		}

		@Override
		public void onBufferReceived(byte[] buffer) {
//			Log.d("Speech", "onBufferReceived");
		}

		@Override
		public void onEndOfSpeech() {
			Log.d("Speech", "onEndOfSpeech");
		}

		@Override
		public void onError(int error) {
			Log.d("Speech", "onError: " + error);
		}

		@Override
		public void onEvent(int eventType, Bundle params) {
			Log.d("Speech", "onEvent");
		}

		@Override
		public void onPartialResults(Bundle partialResults) {
			Log.d("Speech", "onPartialResults");
		}

		@Override
		public void onReadyForSpeech(Bundle params) {
			Log.d("Speech", "onReadyForSpeech");
		}

		@Override
		public void onResults(Bundle results) {
			Log.d("Speech", "onResults");

			ArrayList<String> resultsArray = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
			notifyVoiceListeningResultReady(resultsArray);
		}

		@Override
		public void onRmsChanged(float rmsdB) {
//			Log.d("Speech", "onRmsChanged");
		}
	};

	private void initSpeechRecognizer() {

		speechRecognizer = SpeechRecognizer.createSpeechRecognizer(getApplicationContext());
		speechRecognizer.setRecognitionListener(recognitionListener);
	}

	private void clearSpeechRecognizer() {

		if (speechRecognizer != null) {
			speechRecognizer.stopListening();
			speechRecognizer.cancel();
			speechRecognizer.destroy();
			speechRecognizer = null;
		}
	}


}
